package model.response;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResponseCampaign implements Serializable {
    String _id;

    //chuyển kiểu dữ liệu từ BE trả về theo định dạng của mình theo dạng danh sách
    public static List<ResponseCampaign> FromJson(JSONObject jsonObject) {
        try {
            Gson gson = new Gson();
            String dataProduct = jsonObject.get("results").toString();
            return gson.fromJson(dataProduct, new TypeToken<List<ResponseCampaign>>() {
            }.getType());
        } catch (JSONException jse) {
            jse.printStackTrace();
        }
        return new ArrayList<>();
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}
