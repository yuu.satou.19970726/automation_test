package model.response;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResponseAddressProvince implements Serializable {
    String _id;
    String provinceXX;
    String provinceName;
    String createdAt;
    String updatedAt;

    //chuyển kiểu dữ liệu từ BE trả về theo định dạng của mình theo dạng danh sách
    public static List<ResponseAddressProvince> FromJson(JSONObject jsonObject) {
        try {
            Gson gson = new Gson();
            String dataProduct = jsonObject.get("data").toString();
            return gson.fromJson(dataProduct, new TypeToken<List<ResponseAddressProvince>>() {
            }.getType());
        } catch (JSONException jse) {
            jse.printStackTrace();
        }
        return new ArrayList<>();
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getProvinceXX() {
        return provinceXX;
    }

    public void setProvinceXX(String provinceXX) {
        this.provinceXX = provinceXX;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
