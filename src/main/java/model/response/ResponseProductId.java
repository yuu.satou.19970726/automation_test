package model.response;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResponseProductId implements Serializable {
    String _id;
    String status;
    boolean isOrder;
    List<String> imageUrls;
    boolean isRemoved;
    String name;
    String description;
    String userID;
    String sku;
    String unitID;
    String sellPrice;
    String categoryID;
    String code;
    String slug;
    String createdAt;
    String updatedAt;
    String categoryName;
    String unitName;

    //chuyển kiểu dữ liệu từ BE trả về theo định dạng của mình
    public static ResponseProductId FromJson(JSONObject jsonObject) {
        try {
            Gson gson = new Gson();
            return gson.fromJson(jsonObject.toString(), new TypeToken<ResponseProductId>() {
            }.getType());
        } catch (JSONException jse) {
            jse.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString() {
        return "ResponseProductId{" +
                "_id='" + _id + '\'' +
                ", status='" + status + '\'' +
                ", isOrder=" + isOrder +
                ", imageUrls=" + imageUrls +
                ", isRemoved=" + isRemoved +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", userID='" + userID + '\'' +
                ", sku='" + sku + '\'' +
                ", unitID='" + unitID + '\'' +
                ", sellPrice='" + sellPrice + '\'' +
                ", categoryID='" + categoryID + '\'' +
                ", code='" + code + '\'' +
                ", slug='" + slug + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", unitName='" + unitName + '\'' +
                '}';
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isOrder() {
        return isOrder;
    }

    public void setOrder(boolean order) {
        isOrder = order;
    }

    public List<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(List<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public boolean isRemoved() {
        return isRemoved;
    }

    public void setRemoved(boolean removed) {
        isRemoved = removed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getUnitID() {
        return unitID;
    }

    public void setUnitID(String unitID) {
        this.unitID = unitID;
    }

    public String getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(String sellPrice) {
        this.sellPrice = sellPrice;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }
}
