package model.response;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResponseAddressWard implements Serializable {
    String _id;
    String wardXX;
    String districtXX;
    String wardName;
    String createdAt;
    String updatedAt;

    //chuyển kiểu dữ liệu từ BE trả về theo định dạng của mình theo dạng danh sách
    public static List<ResponseAddressWard> FromJson(JSONObject jsonObject) {
        try {
            Gson gson = new Gson();
            String dataProduct = jsonObject.get("data").toString();
            return gson.fromJson(dataProduct, new TypeToken<List<ResponseAddressWard>>() {
            }.getType());
        } catch (JSONException jse) {
            jse.printStackTrace();
        }
        return new ArrayList<>();
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getWardXX() {
        return wardXX;
    }

    public void setWardXX(String wardXX) {
        this.wardXX = wardXX;
    }

    public String getDistrictXX() {
        return districtXX;
    }

    public void setDistrictXX(String districtXX) {
        this.districtXX = districtXX;
    }

    public String getWardName() {
        return wardName;
    }

    public void setWardName(String wardName) {
        this.wardName = wardName;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
