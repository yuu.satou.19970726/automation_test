package model.response;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class ResponseUserInfo implements Serializable {
    String _id;
    String status;
    String verified;
    String shopName;
    String phone;
    String birthday;
    String email;
    String fullName;
    String gender;

    //chuyển kiểu dữ liệu từ BE trả về theo định dạng của mình
    public static ResponseUserInfo FromJson(JSONObject jsonObject) {
        try {
            Gson gson = new Gson();
            return gson.fromJson(jsonObject.toString(), new TypeToken<ResponseUserInfo>() {
            }.getType());
        } catch (JSONException jse) {
            jse.printStackTrace();
        }
        return null;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVerified() {
        return verified;
    }

    public void setVerified(String verified) {
        this.verified = verified;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
