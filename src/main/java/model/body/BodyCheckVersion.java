package model.body;

import java.io.Serializable;

public class BodyCheckVersion implements Serializable {
    String version;
    String platform;

    public BodyCheckVersion(String version, String platform) {
        this.version = version;
        this.platform = platform;
    }
}
