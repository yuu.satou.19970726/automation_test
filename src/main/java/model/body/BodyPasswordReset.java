package model.body;

import java.io.Serializable;

public class BodyPasswordReset implements Serializable {
    String phone;
    String password;
    String confirmPassword;

    public BodyPasswordReset(String phone, String password, String confirmPassword) {
        this.phone = phone;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }
}
