package model.body;

import java.io.Serializable;

public class BodyPasswordChange implements Serializable {
    String oldPassword;
    String password;
    String confirmPassword;

    public BodyPasswordChange(String oldPassword, String password, String confirmPassword) {
        this.oldPassword = oldPassword;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }
}
