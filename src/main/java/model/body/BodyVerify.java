package model.body;

import java.io.Serializable;

public class BodyVerify implements Serializable {
    String phone;
    String otp;

    public BodyVerify(String phone, String otp) {
        this.phone = phone;
        this.otp = otp;
    }
}
