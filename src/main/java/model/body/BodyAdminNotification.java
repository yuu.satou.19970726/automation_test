package model.body;

import java.io.Serializable;

public class BodyAdminNotification implements Serializable {
    String description;
    String title;

    public BodyAdminNotification(String description, String title) {
        this.description = description;
        this.title = title;
    }
}
