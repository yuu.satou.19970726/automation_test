package model.body;

import java.io.Serializable;

public class BodyRegister implements Serializable {
    String phone;
    String username;
    String password;
    String confirmPassword;

    @Override
    public String toString() {
        return "BodyRegister{" +
                "phone='" + phone + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", confirmPassword='" + confirmPassword + '\'' +
                '}';
    }

    public BodyRegister(String phone, String username, String password, String confirmPassword) {
        this.phone = phone;
        this.username = username;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }
}
