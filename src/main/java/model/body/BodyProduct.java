package model.body;

import java.io.Serializable;
import java.util.List;

public class BodyProduct implements Serializable {
    String name;
    String description;
    String sellPrice;
    String sku;
    String unitID;
    String categoryID;
    List<String> imageUrls;
    boolean isOrder;
    String status;

    public BodyProduct(String name, String description, String sellPrice, String sku, String unitID, String categoryID, List<String> imageUrls, boolean isOrder, String status) {
        this.name = name;
        this.description = description;
        this.sellPrice = sellPrice;
        this.sku = sku;
        this.unitID = unitID;
        this.categoryID = categoryID;
        this.imageUrls = imageUrls;
        this.isOrder = isOrder;
        this.status = status;
    }
}
