package model.body;

import java.io.Serializable;

public class BodyComment implements Serializable {
    String message;
    String campaignID;

    public BodyComment(String message, String campaignID) {
        this.message = message;
        this.campaignID = campaignID;
    }
}
