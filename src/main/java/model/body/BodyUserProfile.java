package model.body;

import java.io.Serializable;

public class BodyUserProfile implements Serializable {
    String shopName;
    String fullName;
    String email;
    String birthday;
    String gender;

    public BodyUserProfile(String shopName, String fullName, String email, String birthday, String gender) {
        this.shopName = shopName;
        this.fullName = fullName;
        this.email = email;
        this.birthday = birthday;
        this.gender = gender;
    }
}
