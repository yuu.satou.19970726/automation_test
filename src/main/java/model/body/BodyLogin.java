package model.body;

import java.io.Serializable;

public class BodyLogin implements Serializable {
    String phone;
    String password;
    String device;
    String version;

    public BodyLogin(String phone, String password, String device, String version) {
        this.phone = phone;
        this.password = password;
        this.device = device;
        this.version = version;
    }
}
