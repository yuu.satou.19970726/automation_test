package api.Reaction;

import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import tool.ClientResource;

import static com.jayway.restassured.RestAssured.given;

//api thông tin sản phẩm
public class PostReactionId {
    private String API_URL = ClientResource.BASE_URL + "marketing/reaction/{id}";
    String idReaction = "";

    public String getAPIUrl() {
        return API_URL;
    }

    public void setIdReaction(String idReaction) {
        this.idReaction = idReaction;
    }

    public Response CallAPI() {
        RequestSpecBuilder builder = new RequestSpecBuilder()
                .setContentType("application/json; charset=UTF-8");
        builder.addHeader("Authorization", "Bearer " + ClientResource.mToken);
        RequestSpecification requestSpec = builder.build();
        //have not Authorization
//        Response response = given().authentication().preemptive().basic("", "")
//                .spec(requestSpec).when().post(API_URL);
        //have Authorization
        Response response = given()
                .spec(requestSpec)
                .pathParam("id", idReaction)
                .when()
                .post(API_URL);

        return response;
    }
}
