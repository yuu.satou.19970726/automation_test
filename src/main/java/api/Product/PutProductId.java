package api.Product;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import model.body.BodyProduct;
import tool.ClientResource;

import java.util.ArrayList;
import java.util.List;

import static com.jayway.restassured.RestAssured.given;

//api tạo sản phẩm
public class PutProductId {
    private String API_URL = ClientResource.BASE_URL + "marketing/product/{id}";
    String idProduct = "";
    private List<String> imageUrls = new ArrayList<>();
    private BodyProduct bodyProduct
            = new BodyProduct("Sony Xperia 5 Mark 2", "Made in Japan", "16790000",
            null, "606589c9a062bb0bdcbd20ed", "60596545515cad03945cd108",
            imageUrls, false, "selling");

    public String getAPIUrl() {
        return API_URL;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public void setBodyProduct(BodyProduct bodyProduct) {
        this.bodyProduct = bodyProduct;
    }

    public BodyProduct getBody() {
        return bodyProduct;
    }

    public Response CallAPI(BodyProduct inputBody)
    {
        RequestSpecBuilder builder = new RequestSpecBuilder()
                .setContentType("application/json; charset=UTF-8");
        builder.addHeader("Authorization", "Bearer " + ClientResource.mToken);
        builder.setBody(inputBody);
        RequestSpecification requestSpec = builder.build();
        //have not Authorization
//        Response response = given().authentication().preemptive().basic("", "")
//                .spec(requestSpec).when().post(API_URL);
        //have Authorization
        Response response = given()
                .spec(requestSpec)
                .pathParam("id", idProduct)
                .when()
                .put(API_URL);
        return response ;
    }
}
