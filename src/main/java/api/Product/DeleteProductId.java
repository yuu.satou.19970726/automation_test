package api.Product;

import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import model.body.BodyProduct;
import tool.ClientResource;

import java.util.ArrayList;
import java.util.List;

import static com.jayway.restassured.RestAssured.given;

public class DeleteProductId {
    private String API_URL = ClientResource.BASE_URL + "marketing/product/{id}";
    String idProduct = "617271fa0eb96d000781fe96";

    public String getAPIUrl() {
        return API_URL;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public Response CallAPI() {
        RequestSpecBuilder builder = new RequestSpecBuilder()
                .setContentType("application/json; charset=UTF-8");
        builder.addHeader("Authorization", "Bearer " + ClientResource.mToken);
        RequestSpecification requestSpec = builder.build();
        //have not Authorization
//        Response response = given().authentication().preemptive().basic("", "")
//                .spec(requestSpec).when().post(API_URL);
        //have Authorization
        Response response = given()
                .spec(requestSpec)
                .pathParam("id", idProduct)
                .when()
                .delete(API_URL);

        return response;
    }
}
