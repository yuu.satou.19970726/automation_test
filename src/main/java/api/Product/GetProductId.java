package api.Product;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import tool.ClientResource;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.StringWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import static com.jayway.restassured.RestAssured.given;

//api thông tin sản phẩm
public class GetProductId {
    private String API_URL = ClientResource.BASE_URL + "marketing/product/{id}";
    String idProduct = "";

    public String getAPIUrl() {
        return API_URL;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public Response CallAPI() {
        RequestSpecBuilder builder = new RequestSpecBuilder()
                .setContentType("application/json; charset=UTF-8");
        builder.addHeader("Authorization", "Bearer " + ClientResource.mToken);
        RequestSpecification requestSpec = builder.build();
        //have not Authorization
//        Response response = given().authentication().preemptive().basic("", "")
//                .spec(requestSpec).when().post(API_URL);
        //have Authorization
        Response response = given()
                .spec(requestSpec)
                .pathParam("id", idProduct)
                .when()
                .get(API_URL);

        return response;
    }
}
