package api.Delivery;

import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import tool.ClientResource;

import java.util.HashMap;

import static com.jayway.restassured.RestAssured.given;

//api thông tin phường/xã ở Việt Nam
public class GetAddressWard {
    private String API_URL = ClientResource.BASE_URL + "delivery/address/ward";

    private HashMap<String, String> hash_map = new HashMap<String, String>();

    public void setAddressWard(String districtXX) {
        //Add param
        hash_map = new HashMap<String, String>();
        hash_map.put("districtXX", districtXX);
    }

    public HashMap<String, String> getHash_map() {
        return hash_map;
    }

    public String getAPIUrl() {
        return API_URL;
    }

    public Response CallAPI() {
        RequestSpecBuilder builder = new RequestSpecBuilder()
                .setContentType("application/json; charset=UTF-8");
        builder.addHeader("Authorization", "Bearer " + ClientResource.mToken);
        //RequestSpecBuilder builder = ClientResource.clientBuilderWithHeader;
        builder.addParams(hash_map);
        RequestSpecification requestSpec = builder.build();
        //have not Authorization
//        Response response = given().authentication().preemptive().basic("", "")
//                .spec(requestSpec).when().post(API_URL);
        //have Authorization
        Response response = given()
                .spec(requestSpec)
                .when()
                .get(API_URL);

        return response;
    }
}
