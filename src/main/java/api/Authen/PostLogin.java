package api.Authen;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import model.body.BodyLogin;
import tool.ClientResource;

import static com.jayway.restassured.RestAssured.given;

//api đăng nhập
public class PostLogin {
    private String API_URL = ClientResource.BASE_URL + "scp/login";
    private BodyLogin bodyLoginBody
            = new BodyLogin("0909090909", "Qwe12345!", "0123456789", "android/1.5.1");

    public String getAPIUrl() {
        return API_URL;
    }

    public void setBodyLoginBody(BodyLogin bodyLoginBody) {
        this.bodyLoginBody = bodyLoginBody;
    }

    public BodyLogin getBody() {
        return bodyLoginBody;
    }

    public Response CallAPI(BodyLogin inputBody)
    {
        RequestSpecBuilder builder = new RequestSpecBuilder()
                .setContentType("application/json; charset=UTF-8");
        builder.setBody(inputBody);
        RequestSpecification requestSpec = builder.build();
        //have not Authorization
        Response response = given().authentication().preemptive().basic("", "")
                .spec(requestSpec).when().post(API_URL);
        //have Authorization
//        Response response = given()
//                .spec(requestSpec).when().post(APIUrl);
        return response ;
    }
}
