package api.Authen;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import model.body.BodyRegister;
import tool.ClientResource;

import static com.jayway.restassured.RestAssured.given;

//api đăng ký
public class PostRegister {
    private String API_URL = ClientResource.BASE_URL + "scp/register";
    private BodyRegister bodyRegister
            = new BodyRegister("0934449999", "Automation Test Staging New New",
            "Qwe12345", "Qwe12345");

    public String getAPIUrl() {
        return API_URL;
    }

    public void setBodyRegister(BodyRegister bodyRegister) {
        this.bodyRegister = bodyRegister;
    }

    public BodyRegister getBody() {
        return bodyRegister;
    }

    public Response CallAPI(BodyRegister inputBody)
    {
        RequestSpecBuilder builder = new RequestSpecBuilder()
                .setContentType("application/json; charset=UTF-8");
        builder.setBody(inputBody);
        RequestSpecification requestSpec = builder.build();
        //have not Authorization
        Response response = given().authentication().preemptive().basic("", "")
                .spec(requestSpec).when().post(API_URL);
        //have Authorization
//        Response response = given()
//                .spec(requestSpec).when().post(API_URL);
        return response ;
    }
}
