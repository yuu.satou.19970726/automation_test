package api.Authen;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import model.body.BodyVerify;
import tool.ClientResource;

import static com.jayway.restassured.RestAssured.given;

//api nhập mã xác nhận(OTP)
public class PostVerify {
    private String API_URL = ClientResource.BASE_URL + "scp/verify";
    private BodyVerify bodyVerify = new BodyVerify("0934449999", "1309");

    public String getAPIUrl() {
        return API_URL;
    }

    public void setBodyVerify(BodyVerify bodyVerify) {
        this.bodyVerify = bodyVerify;
    }

    public BodyVerify getBody() {
        return bodyVerify;
    }

    public Response CallAPI(BodyVerify inputBody)
    {
        RequestSpecBuilder builder = new RequestSpecBuilder()
                .setContentType("application/json; charset=UTF-8");
        builder.setBody(inputBody);
        RequestSpecification requestSpec = builder.build();
        //have not Authorization
        Response response = given().authentication().preemptive().basic("", "")
                .spec(requestSpec).when().post(API_URL);
        //have Authorization
//        Response response = given()
//                .spec(requestSpec).when().post(API_URL);
        return response ;
    }
}
