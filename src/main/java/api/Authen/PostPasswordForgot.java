package api.Authen;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import model.body.BodyPasswordForgot;
import tool.ClientResource;

import static com.jayway.restassured.RestAssured.given;

//api quên mật khẩu
public class PostPasswordForgot {
    private String API_URL = ClientResource.BASE_URL + "scp/password/forgot";
    private BodyPasswordForgot bodyPasswordForgot = new BodyPasswordForgot("0934449999");

    public String getAPIUrl() {
        return API_URL;
    }

    public BodyPasswordForgot getBody() {
        return bodyPasswordForgot;
    }

    public Response CallAPI(BodyPasswordForgot inputBody)
    {
        RequestSpecBuilder builder = new RequestSpecBuilder()
                .setContentType("application/json; charset=UTF-8");
        builder.setBody(inputBody);
        RequestSpecification requestSpec = builder.build();
        //have not Authorization
        Response response = given().authentication().preemptive().basic("", "")
                .spec(requestSpec).when().post(API_URL);
        //have Authorization
//        Response response = given()
//                .spec(requestSpec).when().post(API_URL);
        return response ;
    }
}
