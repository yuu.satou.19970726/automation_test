package api.Authen;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import model.body.BodyPasswordReset;
import tool.ClientResource;

import static com.jayway.restassured.RestAssured.given;

//api thay đổi mật khẩu khi quên mật khẩu cũ
public class PostPasswordReset {
    private String API_URL = ClientResource.BASE_URL + "scp/password/reset";
    private BodyPasswordReset bodyPasswordReset
            = new BodyPasswordReset("0934449999", "Qwe12345", "Qwe12345");

    public String getAPIUrl() {
        return API_URL;
    }

    public BodyPasswordReset getBody() {
        return bodyPasswordReset;
    }

    public Response CallAPI(BodyPasswordReset inputBody)
    {
        RequestSpecBuilder builder = new RequestSpecBuilder()
                .setContentType("application/json; charset=UTF-8");
        builder.setBody(inputBody);
        RequestSpecification requestSpec = builder.build();
        //have not Authorization
        Response response = given().authentication().preemptive().basic("", "")
                .spec(requestSpec).when().post(API_URL);
        //have Authorization
//        Response response = given()
//                .spec(requestSpec).when().post(API_URL);
        return response ;
    }
}
