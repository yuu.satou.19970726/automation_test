package api.Authen;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import model.body.BodyPasswordChange;
import tool.ClientResource;

import static com.jayway.restassured.RestAssured.given;

//api thay đổi mật khẩu
public class PostPasswordChange {
    private String API_URL = ClientResource.BASE_URL + "scp/password/change";
    private BodyPasswordChange bodyPasswordChange
            = new BodyPasswordChange("Qwe12345", "Qwe12345!", "Qwe12345!");

    public String getAPIUrl() {
        return API_URL;
    }

    public BodyPasswordChange getBody() {
        return bodyPasswordChange;
    }

    public Response CallAPI(BodyPasswordChange inputBody)
    {
        RequestSpecBuilder builder = new RequestSpecBuilder()
                .setContentType("application/json; charset=UTF-8");
        builder.addHeader("Authorization", "Bearer " + ClientResource.mToken);
        builder.setBody(inputBody);
        RequestSpecification requestSpec = builder.build();
        //have not Authorization
//        Response response = given().authentication().preemptive().basic("", "")
//                .spec(requestSpec).when().post(API_URL);
        //have Authorization
        Response response = given()
                .spec(requestSpec).when().post(API_URL);
        return response ;
    }
}
