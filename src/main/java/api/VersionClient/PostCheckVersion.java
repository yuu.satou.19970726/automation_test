package api.VersionClient;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import model.body.BodyCheckVersion;
import tool.ClientResource;

import static com.jayway.restassured.RestAssured.given;

public class PostCheckVersion {
    private String API_URL = ClientResource.BASE_URL + "scp/version/check";
    private BodyCheckVersion bodyCheckVersion
            = new BodyCheckVersion("1.5.1", "android");

    public String getAPIUrl() {
        return API_URL;
    }

    public BodyCheckVersion getBody() {
        return bodyCheckVersion;
    }

    public Response CallAPI(BodyCheckVersion inputBody)
    {
        RequestSpecBuilder builder = new RequestSpecBuilder()
                .setContentType("application/json; charset=UTF-8");
        builder.setBody(inputBody);
        RequestSpecification requestSpec = builder.build();
        //have not Authorization
        Response response = given().authentication().preemptive().basic("", "")
                .spec(requestSpec).when().post(API_URL);
        //have Authorization
//        Response response = given()
//                .spec(requestSpec).when().post(APIUrl);
        return response ;
    }
}
