package api.Main;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import tool.ClientResource;

import java.util.HashMap;

import static com.jayway.restassured.RestAssured.given;

//api thông tin về những thiết bị đã dùng tài khoản này đăng nhập
public class GetUserActivity {
    private String API_URL = ClientResource.BASE_URL + "scp/user/activity";

    private HashMap<String, String> hash_map = new HashMap<String, String>();
    public GetUserActivity() {
        //Add param
        hash_map.put("page","1");
        hash_map.put("limit","10");
    }
    public HashMap<String, String> getHash_map() {
        return hash_map;
    }

    public String getAPIUrl() {
        return API_URL;
    }

    public Response CallAPI()
    {
        RequestSpecBuilder builder = new RequestSpecBuilder()
                .setContentType("application/json; charset=UTF-8");
        builder.addHeader("Authorization", "Bearer " + ClientResource.mToken);
        builder.addParams(hash_map);
        RequestSpecification requestSpec = builder.build();
        //have not Authorization
//        Response response = given().authentication().preemptive().basic("", "")
//                .spec(requestSpec).when().post(API_URL);
        //have Authorization
        Response response = given()
                .spec(requestSpec).when().get(API_URL);
        return response ;
    }
}
