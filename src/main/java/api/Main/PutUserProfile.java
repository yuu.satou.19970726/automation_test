package api.Main;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import model.body.BodyUserProfile;
import tool.ClientResource;

import java.util.HashMap;

import static com.jayway.restassured.RestAssured.given;

//api chỉnh sửa thông tin cá nhân của chủ tài khoản
public class PutUserProfile {
    private String API_URL = ClientResource.BASE_URL + "scp/user/profile";
    private BodyUserProfile bodyUserProfile
            = new BodyUserProfile("Android Staging!", "Sherlock Holmes",
            "dungnv101@fpt.com.vn", "1997-07-26", "male");

    public String getAPIUrl() {
        return API_URL;
    }

    public void setBody(BodyUserProfile bodyUserProfile) {
        this.bodyUserProfile = bodyUserProfile;
    }

    public BodyUserProfile getBody() {
        return bodyUserProfile;
    }

    public Response CallAPI(BodyUserProfile inputBody)
    {
        RequestSpecBuilder builder = new RequestSpecBuilder()
                .setContentType("application/json; charset=UTF-8");
        builder.addHeader("Authorization", "Bearer " + ClientResource.mToken);
        builder.setBody(inputBody);
        RequestSpecification requestSpec = builder.build();
        //have not Authorization
//        Response response = given().authentication().preemptive().basic("", "")
//                .spec(requestSpec).when().post(API_URL);
        //have Authorization
        Response response = given()
                .spec(requestSpec).when().put(API_URL);
        return response ;
    }
}
