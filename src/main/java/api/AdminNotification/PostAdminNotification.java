package api.AdminNotification;

import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import model.body.BodyAdminNotification;
import tool.ClientResource;

import static com.jayway.restassured.RestAssured.given;


public class PostAdminNotification {
    private String API_URL = ClientResource.BASE_URL + "admin/notification";
    private BodyAdminNotification bodyAdminNotification
            = new BodyAdminNotification("test socket", "live new 11h!!!");


    public String getAPIUrl() {
        return API_URL;
    }

    public BodyAdminNotification getBody() {
        return bodyAdminNotification;
    }

    public Response CallAPI(BodyAdminNotification inputBody) {
        RequestSpecBuilder builder = new RequestSpecBuilder()
                .setContentType("application/json; charset=UTF-8");
        builder.setBody(inputBody);
        RequestSpecification requestSpec = builder.build();
        //have not Authorization
        Response response = given().authentication().preemptive().basic("", "")
                .spec(requestSpec).when().post(API_URL);
        //have Authorization
//        Response response = given()
//                .spec(requestSpec).when().post(APIUrl);
        return response;
    }
}
