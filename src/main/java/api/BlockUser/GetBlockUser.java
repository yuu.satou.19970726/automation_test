package api.BlockUser;

import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import model.body.BodyProduct;
import tool.ClientResource;

import java.util.ArrayList;
import java.util.List;

import static com.jayway.restassured.RestAssured.given;

//api thông tin sản phẩm
public class GetBlockUser {
    private String API_URL = ClientResource.BASE_URL + "marketing/block/user";

    public String getAPIUrl() {
        return API_URL;
    }

    public Response CallAPI() {
        RequestSpecBuilder builder = new RequestSpecBuilder()
                .setContentType("application/json; charset=UTF-8");
        builder.addHeader("Authorization", "Bearer " + ClientResource.mToken);
        RequestSpecification requestSpec = builder.build();
        //have not Authorization
//        Response response = given().authentication().preemptive().basic("", "")
//                .spec(requestSpec).when().post(API_URL);
        //have Authorization
        Response response = given()
                .spec(requestSpec)
                .when()
                .get(API_URL);

        return response;
    }
}
