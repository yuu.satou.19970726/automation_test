package api.BlockUser;

import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import model.body.BodyBlockUser;
import tool.ClientResource;

import static com.jayway.restassured.RestAssured.given;

//api thông tin sản phẩm
public class DeleteBlockUser {
    private String API_URL = ClientResource.BASE_URL + "marketing/block/user";

    private BodyBlockUser bodyBlockUser = new BodyBlockUser("60eff4ee1457ab000827a194");

    public String getAPIUrl() {
        return API_URL;
    }

    public BodyBlockUser getBody() {
        return bodyBlockUser;
    }

    public Response CallAPI(BodyBlockUser inputBody) {
        RequestSpecBuilder builder = new RequestSpecBuilder()
                .setContentType("application/json; charset=UTF-8");
        builder.addHeader("Authorization", "Bearer " + ClientResource.mToken);
        builder.setBody(inputBody);
        RequestSpecification requestSpec = builder.build();
        //have not Authorization
//        Response response = given().authentication().preemptive().basic("", "")
//                .spec(requestSpec).when().post(API_URL);
        //have Authorization
        Response response = given()
                .spec(requestSpec)
                .when()
                .delete(API_URL);

        return response;
    }
}
