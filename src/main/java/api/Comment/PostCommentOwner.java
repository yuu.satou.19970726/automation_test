package api.Comment;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import model.body.BodyBlockUser;
import model.body.BodyComment;
import tool.ClientResource;

import static com.jayway.restassured.RestAssured.given;

//api tạo sản phẩm
public class PostCommentOwner {
    private String API_URL = ClientResource.BASE_URL + "comment/owner";

    private BodyComment bodyComment = new BodyComment("automation test owner", "campaignID");

    public String getAPIUrl() {
        return API_URL;
    }

    public Response CallAPI(BodyComment inputBody)
    {
        RequestSpecBuilder builder = new RequestSpecBuilder()
                .setContentType("application/json; charset=UTF-8");
        builder.addHeader("Authorization", "Bearer " + ClientResource.mToken);
        builder.setBody(inputBody);
        RequestSpecification requestSpec = builder.build();
        //have not Authorization
//        Response response = given().authentication().preemptive().basic("", "")
//                .spec(requestSpec).when().post(API_URL);
        //have Authorization
        Response response = given()
                .spec(requestSpec).when().post(API_URL);
        return response ;
    }
}
