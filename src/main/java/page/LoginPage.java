package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
    WebDriver mDriver;
    String xPathTextFieldId = "/html/body/div/div/div/div[2]/div/div[1]/div[2]/form/div[1]/div[2]/div/div/input";
    String xPathTextFieldPass = "/html/body/div/div/div/div[2]/div/div[1]/div[2]/form/div[2]/div[2]/div/div/span/input";
    String xPathErrorPass = "/html/body/div/div/div/div[2]/div/div[1]/div[2]/form/div[2]/div[2]/div[2]";
    String xPathButtonLogin = "/html/body/div/div/div/div[2]/div/div[1]/div[2]/form/div[4]/div/div/div/button";

    String classButtonLogin = "ant-btn ant-btn-primary ant-btn-block btn-submit";

    public LoginPage(WebDriver mDriver) {
        this.mDriver = mDriver;
    }


    public void ActionLogin()
    {
        WebElement txtId = mDriver.findElement(By.xpath(xPathTextFieldId));
        WebElement txtPass = mDriver.findElement(By.xpath(xPathTextFieldPass));
        WebElement btnLogin = mDriver.findElement(By.xpath(xPathButtonLogin));
        txtId.sendKeys("0909090909");
        txtPass.sendKeys("Qwe12345");
        btnLogin.click();
    }



    public String GetMessageErrorPass()
    {
        WebElement txtId = mDriver.findElement(By.xpath(xPathTextFieldId));
        WebElement txtPass = mDriver.findElement(By.xpath(xPathTextFieldPass));

        txtId.sendKeys("0909090909");
        txtPass.sendKeys("Qwe12345");

        WebDriverWait wait = new WebDriverWait(mDriver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPathErrorPass)));
        WebElement txtErrorPass = mDriver.findElement(By.xpath(xPathErrorPass));

        return txtErrorPass.getText();

    }
}
