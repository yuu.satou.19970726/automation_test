package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
    WebDriver mDriver;
    String xPathLogin = "/html/body/div/div/div[1]/section[1]/div/header/div/div[2]/a";
    String xPathTextFieldId = "/html/body/div/div/div/div[2]/div/div[1]/div[2]/form/div[1]/div[2]/div/div/input";

    public HomePage(WebDriver mDriver) {
        this.mDriver = mDriver;
    }

    public void ClickLogin()
    {
        WebElement LoginButton = mDriver.findElement(By.xpath(xPathLogin));
        LoginButton.click();
        WebDriverWait wait = new WebDriverWait(mDriver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPathTextFieldId)));

    }

}
