import api.Authen.PostLogin;
import api.Authen.PostRegister;
import api.Authen.PostVerify;
import api.Reaction.PostReactionId;
import com.jayway.restassured.response.Response;
import model.body.BodyLogin;
import model.body.BodyRegister;
import model.body.BodyVerify;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import tool.ClientResource;


public class AutomationTestCountReactions {
    PostLogin postLogin;
    PostReactionId postReactionId;

    @BeforeTest
    public void init() {
        postLogin = new PostLogin();
        postReactionId = new PostReactionId();
    }

    @Test
    public void callAPI() {
        String phone = "0334440000";
        for (int i = 1; i < 100; i++) {
            if (0 <= i && i < 10) {
                phone = "033444000" + i;
            } else if (10 <= i && i < 100){
                phone = "03344400" + i;
            } else if (100 <= i && i < 1000) {
                phone = "0334440" + i;
            }

            BodyLogin bodyLoginBody
                    = new BodyLogin(phone, "Qwe12345", "0123456789", "android/1.5.1");

            postLogin.setBodyLoginBody(bodyLoginBody);
            APIPostLogin();
        }
    }

    public void APIPostLogin() {
        //log
        Reporter.log("Domain " + postLogin.getAPIUrl() + "");
        Reporter.log("Input " + postLogin.getBody() + "");
        //call api
        Response postLoginResponse = postLogin.CallAPI(postLogin.getBody());
        //get status
        int statusCode = postLoginResponse.getStatusCode();
        System.out.println("(POST)" + postLogin.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, postLoginResponse, true, 1);
    }

    public void APIPostReactionId() {
        //log
        Reporter.log("Domain " + postReactionId.getAPIUrl() + "");
        //call api
        Response postLoginResponse = postReactionId.CallAPI();
        //get status
        int statusCode = postLoginResponse.getStatusCode();
        System.out.println("(POST)" + postReactionId.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, postLoginResponse, false, 0);
    }

    private void checkStatus (int statusCode, Response data, boolean checkData, int numCheckData) {
        if (statusCode == 200) {
            JSONObject dataJSON = new JSONObject(data.body().asString());
            System.out.println("Response body: \n" + dataJSON);
            if (checkData){
                accessTokenTemp(dataJSON);
                postReactionId.setIdReaction("61a48e51fd80850008a47058");
                APIPostReactionId();
            }
        } else {
            System.out.println("Response error body: \n" + data.asString());
//            Assert.assertEquals(statusCode, 200);
//            Reporter.log("Output " + data.asString() + "");
        }
    }

    //accessToken temp
    private void accessTokenTemp (JSONObject jsonObject) {
        ClientResource.mToken = jsonObject.getJSONObject("data").getString("accessToken");
    }

    @AfterTest
    public void tearDown() {
        //mDriver.quit();
    }

}
