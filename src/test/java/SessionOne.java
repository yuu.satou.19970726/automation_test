import page.HomePage;
import page.LoginPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;



public class SessionOne {
    WebDriver mDriver;
    @BeforeTest
    public void init()
    {
        //WebDriverManager.chromedriver().setup();
        WebDriverManager.chromedriver().version("94.0.4606.61").setup();
        mDriver = new ChromeDriver();
        mDriver.get("https://dev.xinhxinh.live/");
    }

    @Test
    public void TC001()
    {
        HomePage mHomePage = new HomePage(mDriver);
        mHomePage.ClickLogin();
        LoginPage mLoginPage = new LoginPage(mDriver);
        mLoginPage.ActionLogin();
        //Assert.assertEquals(mLoginPage.GetMessageErrorPass(),"Mật khẩu phải ít nhất 8 ký tự, bao gồm cả chữ hoa, chữ thường và số!");
    }

    @AfterTest
    public void tearDown()
    {
        //mDriver.quit();
    }

}
