import api.Authen.PostLogin;
import api.Main.GetUserInfo;
import api.Main.PutUserProfile;
import com.jayway.restassured.response.Response;
import model.body.BodyUserProfile;
import model.response.ResponseUserInfo;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import tool.ClientResource;

// B1. (PostLogin) đăng nhập -> lấy accessToken(thông tin về chủ tài khoản).
// B2. (GetUserInfo) sử dụng accessToken để lấy thông tin cơ bản của chủ tài khoản.
// B3. (PutUserProfile) dựa trên thông tin lấy được từ B2 tiến hành thay đổi thông tin của chủ tài khoản.
public class AutomationTestEditProfile {
    PostLogin postLogin;
    GetUserInfo getUserInfo;
    PutUserProfile putUserProfile;

    @BeforeTest
    public void init() {
        postLogin = new PostLogin();
        getUserInfo = new GetUserInfo();
        putUserProfile = new PutUserProfile();
    }

    @Test(priority = 1)
    public void APIPostLogin() {
        //log
        Reporter.log("Domain " + postLogin.getAPIUrl() + "");
        Reporter.log("Input " + postLogin.getBody() + "");
        //call api
        Response postLoginResponse = postLogin.CallAPI(postLogin.getBody());
        //get status
        int statusCode = postLoginResponse.getStatusCode();
        System.out.println("(POST)" + postLogin.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, postLoginResponse, true, 1);
    }

    @Test(priority = 2)
    public void APIGetUserInfo() {
        //log
        Reporter.log("Domain " + getUserInfo.getAPIUrl() + "");
        //call api
        Response getUserInfoResponse = getUserInfo.CallAPI();
        //get status
        int statusCode = getUserInfoResponse.getStatusCode();
        System.out.println("(GET)" + getUserInfo.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, getUserInfoResponse, true, 2);
    }

    @Test(priority = 3)
    public void APIPutUserProfile() {
        //log
        Reporter.log("Domain " + putUserProfile.getAPIUrl() + "");
        Reporter.log("Input " + putUserProfile.getBody() + "");
        //call api
        Response putUserProfileResponse = putUserProfile.CallAPI(putUserProfile.getBody());
        //get status
        int statusCode = putUserProfileResponse.getStatusCode();
        System.out.println("(PUT)" + putUserProfile.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, putUserProfileResponse, false, 0);
    }

    //được tạo ra nhầm mục đích rút gọn những đoạn code được dùng đi dùng lại nhiều lần.
    //giá trị của checkData:
    //_ (true) người code muốn lấy dữ liệu mà phía BE trả về để phục vụ cho vấn đề của người code.
    //_ (false) không cần dữ liệu mà BE trả về.
    //giá trị của numCheckData: hỗ trợ cho checkData lấy đúng dữ liệu mà BE trả về đúng với api được gọi trong thời điểm đó.
    private void checkStatus(int statusCode, Response data, boolean checkData, int numCheckData) {
        if (statusCode == 200) {
            JSONObject dataJSON = new JSONObject(data.body().asString());
            System.out.println("Response body: \n" + dataJSON);
            if (checkData) {
                //switch giống với if else nhưng gọn hơn phù hợp với trường hợp chỉ truyền 1 kiểu giá trị duy nhất.
                switch (numCheckData) {
                    //nếu giá trị của numCheckData là 1:
                    //_ api login đang được gọi, sử dụng đoạn code phù hợp để có thể lấy dữ liệu mà BE trả về
                    case 1:
                        accessTokenTemp(dataJSON);
                        break;
                    //nếu giá trị của numCheckData là 2:
                    //_ api cập nhật thông tin cá nhân đang được gọi
                    case 2:
                        //model để hứng dữ liệu từ phía BE trả về
                        ResponseUserInfo responseUserInfo = ResponseUserInfo.FromJson(dataJSON.getJSONObject("data"));
                        //lấy dữ liệu được hừng từ phía BE truyền vào cho phần chỉnh sửa thông tin cá nhân
                        String shopName = "";
                        String fullName = "";
                        String eMail= "";
                        String birthDay = "";
                        String gender= "";
                        if (responseUserInfo.getShopName() == null){
                            shopName = "not found shop name!";
                        } else {
                            shopName = responseUserInfo.getShopName();
                        }

                        if (responseUserInfo.getFullName() == null){
                            fullName = "new full name new!";
                        } else {
                            fullName = responseUserInfo.getFullName();
                        }

                        if (responseUserInfo.getEmail() == null){
                            eMail = "new@gmail.new!";
                        } else {
                            eMail = responseUserInfo.getEmail();
                        }

                        if (responseUserInfo.getBirthday() == null){
                            birthDay = "1997-07-26";
                        } else {
                            birthDay = responseUserInfo.getBirthday();
                        }

                        if (responseUserInfo.getGender() == null){
                            gender = "male";
                        } else {
                            gender = responseUserInfo.getGender();
                        }

                        BodyUserProfile bodyUserProfile
                                = new BodyUserProfile(shopName , fullName, eMail,
                                birthDay, gender);
                        //lấy dữ liệu được tạo cho việc chỉnh sửa thông tin cá nhân truyền vào body thay đổi body mặc định
                        putUserProfile.setBody(bodyUserProfile);
                        break;
                }
                //ví dụ về code if
                //nếu giá trị của numCheckData là 1:
                //_ api login đang được gọi, sử dụng đoạn code phù hợp để có thể lấy dữ liệu mà BE trả về
//                if (numCheckData == 1){
//                    accessTokenTemp(dataJSON);
//                }
                //nếu giá trị của numCheckData là 2:
                //_ api cập nhật thông tin cá nhân đang được gọi
//                else if (numCheckData == 2) {
//                    ResponseUserInfo responseUserInfo = ResponseUserInfo.FromJson(dataJSON.getJSONObject("data"));
//                    BodyUserProfile bodyUserProfile
//                            = new BodyUserProfile(responseUserInfo.getShopName() , responseUserInfo.getFullName(),
//                            responseUserInfo.getEmail(), responseUserInfo.getBirthday(),
//                            responseUserInfo.getGender());
//                    putUserProfile.setBody(bodyUserProfile);
//                }
//                else {
//
//                }
            }
        } else {
            System.out.println("Response error body: \n" + data.asString());
            Assert.assertEquals(statusCode, 200);
            Reporter.log("Output " + data.asString() + "");
        }
    }

    //cập nhật và lưu tạm accessToken vừa lấy về từ phía BE
    private void accessTokenTemp(JSONObject jsonObject) {
        //{
        // "data": {
        //      "accessToken":"Authorization"
        //    }
        // }

        //jsonObject.getJSONObject("data(key)"): dựa theo (key) để biết mục cần lấy dữ liệu
        //jsonObject.getJSONObject("data").getString("accessToken(key)"): dựa theo (key) lấy giá trị muốn trong phần dữ liệu
        ClientResource.mToken = jsonObject.getJSONObject("data").getString("accessToken");
    }

    @AfterTest
    public void tearDown() {
        //mDriver.quit();
    }

}
