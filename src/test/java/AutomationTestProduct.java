import api.Product.DeleteProductId;
import api.Product.GetProductId;
import api.Product.PostProductId;
import api.Product.PutProductId;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import model.response.ResponseProductId;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import tool.ClientResource;

// B1. (PostProductId) tạo sản phẩm -> sau đó lấy _id từ BE trả về truyền vào cho (GetProductId).
// B2. (GetProductId) lấy thông tin sản phẩm -> tiếp tục lấy _id từ BE trả về truyền vào cho (PutProductId).
// B3. (PutProductId) chỉnh sửa thông tin sản phẩm -> tiếp tục lấy _id từ BE trả về truyền vào cho (GetProductId).
// B3. (GetProductId) lấy thông tin sản phẩm -> tiếp tục lấy _id từ BE trả về truyền vào cho (DeleteProductId).
// B3. (DeleteProductId) xoá sản phẩm.
public class AutomationTestProduct {
    PostProductId postProductId;
    GetProductId getProductId;
    PutProductId putProductId;
    DeleteProductId deleteProductId;

    @BeforeTest
    public void init() {
        postProductId = new PostProductId();
        getProductId = new GetProductId();
        putProductId = new PutProductId();
        deleteProductId = new DeleteProductId();
    }

    @Test(priority = 1)
    public void APIPostProductId() {
        //log
        Reporter.log("Domain " + postProductId.getAPIUrl() + "");
        Reporter.log("Input " + postProductId.getBody() + "");
        //call api
        Response postProductIdResponse = postProductId.CallAPI(postProductId.getBody());
        //get status
        int statusCode = postProductIdResponse.getStatusCode();
        System.out.println("(POST)" + postProductId.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, postProductIdResponse, true, 1);
    }

    @Test(priority = 2)
    public void APIGetProductIdWhenCreate() {
        //log
        Reporter.log("Domain " + getProductId.getAPIUrl() + "");
//        Reporter.log("Input " + getProductId.getHash_map() + "");
        //call api
        Response getProductIdWhenCreateResponse = getProductId.CallAPI();
        //get status
        int statusCode = getProductIdWhenCreateResponse.getStatusCode();
        System.out.println("(GET)" + getProductId.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, getProductIdWhenCreateResponse, true, 2);
        RestAssured.reset();
    }

    @Test(priority = 3)
    public void APIPutProductId() {
        //log
        Reporter.log("Domain " + putProductId.getAPIUrl() + "");
//        Reporter.log("Input " + getProductId.getHash_map() + "");
        //call api
        Response putProductIdResponse = putProductId.CallAPI(putProductId.getBody());
        //get status
        int statusCode = putProductIdResponse.getStatusCode();
        System.out.println("(PUT)" + putProductId.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, putProductIdResponse, true, 3);
        RestAssured.reset();
    }

    @Test(priority = 4)
    public void APIGetProductIdWhenUpdate() {
        //log
        Reporter.log("Domain " + getProductId.getAPIUrl() + "");
//        Reporter.log("Input " + getProductId.getHash_map() + "");
        //call api
        Response getProductIdWhenUpdateResponse = getProductId.CallAPI();
        //get status
        int statusCode = getProductIdWhenUpdateResponse.getStatusCode();
        System.out.println("(GET)" + getProductId.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, getProductIdWhenUpdateResponse, true, 4);
        RestAssured.reset();
    }

    @Test(priority = 5)
    public void APIDeleteProductId() {
        //log
        Reporter.log("Domain " + deleteProductId.getAPIUrl() + "");
//        Reporter.log("Input " + deleteProductId.getHash_map() + "");
        //call api
        Response deleteProductIdResponse = deleteProductId.CallAPI();
        //get status
        int statusCode = deleteProductIdResponse.getStatusCode();
        System.out.println("(DELETE)" + deleteProductId.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, deleteProductIdResponse, false, 0);
        RestAssured.reset();
    }

    private void checkStatus (int statusCode, Response data, boolean getData, int numGetData) {
        if (statusCode == 200) {
            JSONObject dataJSON = new JSONObject(data.body().asString());
            System.out.println("Response body: \n" + dataJSON);
            if (getData){
                switch (numGetData){
                    case 2:
                        ResponseProductId responseProductIdInfoWhenCreate = ResponseProductId.FromJson(dataJSON.getJSONObject("data"));
                        putProductId.setIdProduct(responseProductIdInfoWhenCreate.get_id());
                        break;
                    case 4:
                        ResponseProductId responseProductIdInfoWhenUpdate = ResponseProductId.FromJson(dataJSON.getJSONObject("data"));
                        deleteProductId.setIdProduct(responseProductIdInfoWhenUpdate.get_id());
                        break;
                    default:
                        ResponseProductId responseProductIdCreate = ResponseProductId.FromJson(dataJSON.getJSONObject("data"));
                        getProductId.setIdProduct(responseProductIdCreate.get_id());
                        break;
                }
            }
        } else {
            System.out.println("Response error body: \n" + data.asString());
            Assert.assertEquals(statusCode, 200);
            Reporter.log("Output " + data.asString() + "");
        }
    }

    //accessToken temp
    private void accessTokenTemp (JSONObject jsonObject) {
        ClientResource.mToken = jsonObject.getJSONObject("data").getString("accessToken");
    }

    @AfterTest
    public void tearDown() {
        //mDriver.quit();
    }

}
