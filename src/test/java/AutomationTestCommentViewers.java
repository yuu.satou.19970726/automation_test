import api.Authen.PostLogin;
import api.Comment.PostCommentViewer;
import api.Reaction.PostReactionId;
import com.jayway.restassured.response.Response;
import model.body.BodyComment;
import model.body.BodyLogin;
import org.json.JSONObject;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import tool.ClientResource;


public class AutomationTestCommentViewers {
    int count = 1;
    PostLogin postLogin;
    PostCommentViewer postCommentViewer;

    @BeforeTest
    public void init() {
        postLogin = new PostLogin();
        postCommentViewer = new PostCommentViewer();
    }

    @Test(invocationCount = 99, skipFailedInvocations = true)
    public void callAPI() {
        String phone = "0334440000";

        if (0 <= count && count < 10) {
            phone = "033444000" + count;
        } else if (10 <= count && count < 100){
            phone = "03344400" + count;
        } else if (100 <= count && count < 1000) {
            phone = "0334440" + count;
        }

        BodyLogin bodyLoginBody
                = new BodyLogin(phone, "Qwe12345", "0123456789", "android/1.5.1");

        postLogin.setBodyLoginBody(bodyLoginBody);
        APIPostLogin();
    }

    public void APIPostLogin() {
        //log
        Reporter.log("Domain " + postLogin.getAPIUrl() + "");
        Reporter.log("Input " + postLogin.getBody() + "");
        //call api
        Response postLoginResponse = postLogin.CallAPI(postLogin.getBody());
        //get status
        int statusCode = postLoginResponse.getStatusCode();
        System.out.println("(POST)" + postLogin.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, postLoginResponse, true, 1);
    }

    public void APIPostCommentViewer() {
        //log
        Reporter.log("Domain " + postCommentViewer.getAPIUrl() + "");
        //call api
        Response postLoginResponse = postCommentViewer.CallAPI();
        //get status
        int statusCode = postLoginResponse.getStatusCode();
        System.out.println("(POST)" + postCommentViewer.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, postLoginResponse, false, 0);
    }

    private void checkStatus (int statusCode, Response data, boolean checkData, int numCheckData) {
        if (statusCode == 200) {
            JSONObject dataJSON = new JSONObject(data.body().asString());
            System.out.println("Response body: \n" + dataJSON);
            if (checkData){
                accessTokenTemp(dataJSON);
                BodyComment bodyComment = new BodyComment(count + "", "61a58ca8e205540007801801");
                count = count + 1;
                postCommentViewer.setBodyComment(bodyComment);
                APIPostCommentViewer();
            }
        } else {
            System.out.println("Response error body: \n" + data.asString());
//            Assert.assertEquals(statusCode, 200);
//            Reporter.log("Output " + data.asString() + "");
        }
    }

    //accessToken temp
    private void accessTokenTemp (JSONObject jsonObject) {
        ClientResource.mToken = jsonObject.getJSONObject("data").getString("accessToken");
    }

    @AfterTest
    public void tearDown() {
        //mDriver.quit();
    }

}
