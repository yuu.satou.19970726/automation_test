import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;

import api.BlockUser.DeleteBlockUser;
import api.BlockUser.PostBlockUser;
import api.Delivery.GetAddressDistrict;
import api.Delivery.GetAddressProvince;
import api.Delivery.GetAddressWard;
import api.Product.DeleteProductId;
import api.Product.GetProductId;
import api.Product.PostProductId;
import api.Product.PutProductId;
import com.jayway.restassured.RestAssured;
import model.body.BodyProduct;
import model.response.ResponseAddressDistrict;
import model.response.ResponseAddressProvince;
import model.response.ResponseProductId;
import model.response.ResponseUserInfo;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.jayway.restassured.response.Response;
import tool.ClientResource;

import java.util.ArrayList;
import java.util.List;


public class AutomationTestMultiple {
    PostProductId postProductId;
    GetProductId getProductId;
    PutProductId putProductId;
    DeleteProductId deleteProductId;

    @BeforeTest
    public void init() {
        postProductId = new PostProductId();
        getProductId = new GetProductId();
        putProductId = new PutProductId();
        deleteProductId = new DeleteProductId();
    }

    @Test(priority = 1)
    public void APIPostProductId() {
        //log
        Reporter.log("Domain " + postProductId.getAPIUrl() + "");
        Reporter.log("Input " + postProductId.getBody() + "");
        //call api
        Response checkVersionResponse = postProductId.CallAPI(postProductId.getBody());
        //get status
        int statusCode = checkVersionResponse.getStatusCode();
        System.out.println("(POST)" + postProductId.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, checkVersionResponse, true, 1);
    }

    @Test(priority = 2)
    public void APIGetProductIdWhenCreate() {
        //log
        Reporter.log("Domain " + getProductId.getAPIUrl() + "");
//        Reporter.log("Input " + getProductId.getHash_map() + "");
        //call api
        Response checkLoginResponse = getProductId.CallAPI();
        //get status
        int statusCode = checkLoginResponse.getStatusCode();
        System.out.println("(GET)" + getProductId.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, checkLoginResponse, true, 2);
        RestAssured.reset();
    }

    @Test(priority = 3)
    public void APIPutProductId() {
        //log
        Reporter.log("Domain " + putProductId.getAPIUrl() + "");
//        Reporter.log("Input " + getProductId.getHash_map() + "");
        //call api
        Response checkLoginResponse = putProductId.CallAPI(putProductId.getBody());
        //get status
        int statusCode = checkLoginResponse.getStatusCode();
        System.out.println("(PUT)" + putProductId.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, checkLoginResponse, true, 3);
        RestAssured.reset();
    }

    @Test(priority = 4)
    public void APIGetProductIdWhenUpdate() {
        //log
        Reporter.log("Domain " + getProductId.getAPIUrl() + "");
//        Reporter.log("Input " + getProductId.getHash_map() + "");
        //call api
        Response checkLoginResponse = getProductId.CallAPI();
        //get status
        int statusCode = checkLoginResponse.getStatusCode();
        System.out.println("(GET)" + getProductId.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, checkLoginResponse, true, 4);
        RestAssured.reset();
    }

    @Test(priority = 5)
    public void APIDeleteProductId() {
        //log
        Reporter.log("Domain " + deleteProductId.getAPIUrl() + "");
//        Reporter.log("Input " + deleteProductId.getHash_map() + "");
        //call api
        Response checkLoginResponse = deleteProductId.CallAPI();
        //get status
        int statusCode = checkLoginResponse.getStatusCode();
        System.out.println("(DELETE)" + deleteProductId.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, checkLoginResponse, false, 0);
        RestAssured.reset();
    }

    private void checkStatus (int statusCode, Response data, boolean getData, int numGetData) {
        if (statusCode == 200) {
            JSONObject dataJSON = new JSONObject(data.body().asString());
            System.out.println("Response body: \n" + dataJSON);
            if (getData){
                switch (numGetData){
                    case 2:
                        ResponseProductId responseProductIdInfoWhenCreate = ResponseProductId.FromJson(dataJSON.getJSONObject("data"));
                        putProductId.setIdProduct(responseProductIdInfoWhenCreate.get_id());
                        break;
                    case 4:
                        ResponseProductId responseProductIdInfoWhenUpdate = ResponseProductId.FromJson(dataJSON.getJSONObject("data"));
                        deleteProductId.setIdProduct(responseProductIdInfoWhenUpdate.get_id());
                        break;
                    default:
                        ResponseProductId responseProductIdCreate = ResponseProductId.FromJson(dataJSON.getJSONObject("data"));
                        getProductId.setIdProduct(responseProductIdCreate.get_id());
                        break;
                }
            }
        } else {
            System.out.println("Response error body: \n" + data.asString());
            Assert.assertEquals(statusCode, 200);
            Reporter.log("Output " + data.asString() + "");
        }
    }

    //accessToken temp
    private void accessTokenTemp (JSONObject jsonObject) {
        ClientResource.mToken = jsonObject.getJSONObject("data").getString("accessToken");
    }

    @AfterTest
    public void tearDown() {
        //mDriver.quit();
    }

}
