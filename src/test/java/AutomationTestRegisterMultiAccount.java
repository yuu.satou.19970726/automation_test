import api.Authen.PostRegister;
import api.Authen.PostVerify;
import api.Delivery.GetAddressDistrict;
import api.Delivery.GetAddressProvince;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import model.body.BodyComment;
import model.body.BodyRegister;
import model.body.BodyVerify;
import model.response.ResponseAddressProvince;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import tool.ClientResource;

import java.util.List;


public class AutomationTestRegisterMultiAccount {
    PostRegister postRegister;
    PostVerify postVerify;

    int count = 1;

    String phone = "0334440000";

    @BeforeTest
    public void init() {
        postRegister = new PostRegister();
        postVerify = new PostVerify();
    }

    @Test(invocationCount = 99, skipFailedInvocations = true)
    public void callAPI() {
//        for (int i = 1; i < 100; i++) {
//            if (0 <= i && i < 10) {
//                phone = "033444000" + i;
//            } else if (10 <= i && i < 100){
//                phone = "03344400" + i;
//            } else if (100 <= i && i < 1000) {
//                phone = "0334440" + i;
//            }
//            String userName = "Vote Man " + i;
//
//            BodyRegister bodyRegister
//                    = new BodyRegister(phone, userName,
//                    "Qwe12345", "Qwe12345");
//            postRegister.setBodyRegister(bodyRegister);
//            APIPostRegister();
//        }
        if (0 <= count && count < 10) {
            phone = "033444000" + count;
        } else if (10 <= count && count < 100){
            phone = "03344400" + count;
        } else if (100 <= count && count < 1000) {
            phone = "0334440" + count;
        }
        String userName = "Vote Man " + count;

        BodyRegister bodyRegister
                = new BodyRegister(phone, userName,
                "Qwe12345", "Qwe12345");
        postRegister.setBodyRegister(bodyRegister);
        APIPostRegister();
    }

    public void APIPostRegister() {
        //log
        Reporter.log("Domain " + postRegister.getAPIUrl() + "");
//        Reporter.log("Input " + postRegister.getBody() + "");
        //call api
        Response postLoginResponse = postRegister.CallAPI(postRegister.getBody());
        //get status
        int statusCode = postLoginResponse.getStatusCode();
        System.out.println("(POST)" + postRegister.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, postLoginResponse, true, 1);
    }

    public void APIPostVerify() {
        //log
        Reporter.log("Domain " + postVerify.getAPIUrl() + "");
        Reporter.log("Input " + postVerify.getBody() + "");
        //call api
        Response postLoginResponse = postVerify.CallAPI(postVerify.getBody());
        //get status
        int statusCode = postLoginResponse.getStatusCode();
        System.out.println("(POST)" + postVerify.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, postLoginResponse, false, 0);
    }

    private void checkStatus (int statusCode, Response data, boolean checkData, int numCheckData) {
        if (statusCode == 200) {
            JSONObject dataJSON = new JSONObject(data.body().asString());
            System.out.println("Response body: \n" + dataJSON);
            if (checkData){
                count = count + 1;
                BodyVerify bodyVerify = new BodyVerify(phone, "1309");
                postVerify.setBodyVerify(bodyVerify);
                APIPostVerify();
            }
        } else {
            System.out.println("Response error body: \n" + data.asString());
            Assert.assertEquals(statusCode, 200);
            Reporter.log("Output " + data.asString() + "");
        }
    }

    //accessToken temp
    private void accessTokenTemp (JSONObject jsonObject) {
        ClientResource.mToken = jsonObject.getJSONObject("data").getString("accessToken");
    }

    @AfterTest
    public void tearDown() {
        //mDriver.quit();
    }

}
