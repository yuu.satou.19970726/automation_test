import api.Customer.DeleteCustomerId;
import api.Customer.GetCustomerList;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import model.body.BodyComment;
import model.response.ResponseCustomers;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import tool.ClientResource;

import java.util.List;


public class AutomationTestClearCustomer {
    GetCustomerList getCustomerList;
    DeleteCustomerId deleteCustomerId;

    @BeforeTest
    public void init() {
        getCustomerList = new GetCustomerList();
        deleteCustomerId = new DeleteCustomerId();
    }

    @Test(skipFailedInvocations = true)
    public void callAPI() {
        //log
        Reporter.log("Domain " + getCustomerList.getAPIUrl() + "");
        //call api
        BodyComment bodyComment = new BodyComment("automation test owner!!!", "6178d69ccb457e000885195c");
        Response response = getCustomerList.CallAPI();
        //get status
        int statusCode = response.getStatusCode();
        System.out.println("(GET)" + getCustomerList.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, response, true);
    }

    public void APIDeleteCustomerId() {
        //log
        Reporter.log("Domain " + deleteCustomerId.getAPIUrl() + "");
//        Reporter.log("Input " + deleteCustomerId.getHash_map() + "");
        //call api
        Response getAddressDistrictResponse = deleteCustomerId.CallAPI();
        //get status
        int statusCode = getAddressDistrictResponse.getStatusCode();
        System.out.println("(DELETE)" + deleteCustomerId.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, getAddressDistrictResponse, false);
        RestAssured.reset();
    }

    private void checkStatus(int statusCode, Response data, boolean getValue) {
        if (statusCode == 200) {
            JSONObject dataJSON = new JSONObject(data.body().asString());
            System.out.println("Response body: \n" + dataJSON);
            if (getValue) {
                List<ResponseCustomers> responseCustomers
                        = ResponseCustomers.FromJson(dataJSON.getJSONObject("data"));
                if (responseCustomers != null && responseCustomers.size() > 0) {
                    for (int i = 0; i < responseCustomers.size(); i++) {
                        if (!responseCustomers.get(i).get_id().equals("61a5f8b8e9ba440007398648") &&
                                !responseCustomers.get(i).get_id().equals("61a5f530e9ba4400073985bc")) {
                            deleteCustomerId.setIdCustomer(responseCustomers.get(i).get_id());
                            APIDeleteCustomerId();
                        }
                    }
                }
            }
        } else {
            System.out.println("Response error body: \n" + data.asString());
            Assert.assertEquals(statusCode, 200);
            Reporter.log("Output " + data.asString() + "");
        }
    }

    //accessToken temp
    private void accessTokenTemp(JSONObject jsonObject) {
        ClientResource.mToken = jsonObject.getJSONObject("data").getString("accessToken");
    }

    @AfterTest
    public void tearDown() {
        //mDriver.quit();
    }

}
