import api.Category.GetCategory;
import api.Comment.GetCommentList;
import api.Comment.PostCommentOwner;
import api.Comment.PostCommentViewer;
import api.Customer.DeleteCustomerId;
import api.Customer.GetCustomerList;
import api.Delivery.GetAddressDistrict;
import api.Delivery.GetAddressProvince;
import api.Marketing.DeleteCampaignId;
import api.Marketing.GetCampaignList;
import api.Product.DeleteProductId;
import api.Product.GetProductId;
import api.Product.GetProductList;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import model.body.BodyComment;
import model.response.ResponseAddressProvince;
import model.response.ResponseCampaign;
import model.response.ResponseCustomers;
import model.response.ResponseProductList;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import tool.ClientResource;

import java.util.List;


public class AutomationTestSingle {
    int count = 1;
    PostCommentViewer postCommentViewer;

    @BeforeTest
    public void init() {
        postCommentViewer = new PostCommentViewer();
    }

    @Test(invocationCount = 1000, skipFailedInvocations = true)
    public void callAPI() {
        //log
        Reporter.log("Domain " + postCommentViewer.getAPIUrl() + "");

        BodyComment bodyComment = new BodyComment("test post man 2 " + count + "", "61cd1672b25d08000799f0d3");
        count = count + 1;
        postCommentViewer.setBodyComment(bodyComment);

        //call api
        Response response = postCommentViewer.CallAPI();
        //get status
        int statusCode = response.getStatusCode();
        System.out.println("(POST)" + postCommentViewer.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, response, true);
    }

    private void checkStatus(int statusCode, Response data, boolean getValue) {
        if (statusCode == 200) {
            JSONObject dataJSON = new JSONObject(data.body().asString());
            System.out.println("Response body: \n" + dataJSON);
            if (getValue) {
//                List<ResponseCampaign> responseCampaigns
//                        = ResponseCampaign.FromJson(dataJSON.getJSONObject("data"));
//                if (responseCampaigns != null && responseCampaigns.size() > 0) {
//                    for (int i = 0; i < responseCampaigns.size(); i++) {
//                        deleteCampaignId.setIdCustomer(responseCampaigns.get(i).get_id());
//                        APIDeleteCustomerId();
//                    }
//                }
            }
        } else {
            System.out.println("Response error body: \n" + data.asString());
            Assert.assertEquals(statusCode, 200);
            Reporter.log("Output " + data.asString() + "");
        }
    }

    //accessToken temp
    private void accessTokenTemp(JSONObject jsonObject) {
        ClientResource.mToken = jsonObject.getJSONObject("data").getString("accessToken");
    }

    @AfterTest
    public void tearDown() {
        //mDriver.quit();
    }

}
