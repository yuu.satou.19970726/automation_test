import api.Delivery.GetAddressDistrict;
import api.Delivery.GetAddressProvince;
import api.Delivery.GetAddressWard;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import model.response.ResponseAddressDistrict;
import model.response.ResponseAddressProvince;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import tool.ClientResource;

import java.util.List;

// B1. (GetAddressProvince) lấy thông tin tỉnh/thành ở Việt Nam -> lấy provinceXX từ BE.
// B2. (GetAddressDistrict) sử dụng provinceXX để lấy thông tin quận\huyện ở Việt Nam -> lấy districtXX từ BE.
// B3. (GetAddressWard) sử dụng districtXX để lấy phường/xã ở Việt Nam.
public class AutomationTestAddress {
    GetAddressProvince getAddressProvince;
    GetAddressDistrict getAddressDistrict;
    GetAddressWard getAddressWard;

    @BeforeTest
    public void init() {
        getAddressProvince = new GetAddressProvince();
        getAddressDistrict = new GetAddressDistrict();
        getAddressWard = new GetAddressWard();
    }

    @Test(priority = 1)
    public void APIGetAddressProvince() {
        //log
        Reporter.log("Domain " + getAddressProvince.getAPIUrl() + "");
//        Reporter.log("Input " + getAddressProvince.getBody() + "");
        //call api
        Response getAddressProvinceResponse = getAddressProvince.CallAPI();
        //get status
        int statusCode = getAddressProvinceResponse.getStatusCode();
        System.out.println("(GET)" + getAddressProvince.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, getAddressProvinceResponse, true, 1);
    }

    @Test(priority = 2)
    public void APIGetAddressDistrict() {
        //log
        Reporter.log("Domain " + getAddressDistrict.getAPIUrl() + "");
        Reporter.log("Input " + getAddressDistrict.getHash_map() + "");
        //call api
        Response getAddressDistrictResponse = getAddressDistrict.CallAPI();
        //get status
        int statusCode = getAddressDistrictResponse.getStatusCode();
        System.out.println("(GET)" + getAddressDistrict.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, getAddressDistrictResponse, true, 2);
        RestAssured.reset();
    }

    @Test(priority = 3)
    public void APIGetAddressWard() {
        //log
        Reporter.log("Domain " + getAddressWard.getAPIUrl() + "");
        Reporter.log("Input " + getAddressWard.getHash_map() + "");
        //call api
        Response getAddressWardResponse = getAddressWard.CallAPI();
        //get status
        int statusCode = getAddressWardResponse.getStatusCode();
        System.out.println("(GET)" + getAddressWard.getAPIUrl() + "\n");
        System.out.println("Response status: " + statusCode + "\n");
        //get data from status
        checkStatus(statusCode, getAddressWardResponse, false, 0);
        RestAssured.reset();
    }

    private void checkStatus (int statusCode, Response data, boolean getData, int numGetData) {
        if (statusCode == 200) {
            JSONObject dataJSON = new JSONObject(data.body().asString());
            System.out.println("Response body: \n" + dataJSON);
            if (getData){
                switch (numGetData){
                    case 1:
                        List<ResponseAddressProvince> responseAddressProvinceList
                                = ResponseAddressProvince.FromJson(dataJSON);
                        if (responseAddressProvinceList != null && responseAddressProvinceList.size() > 0){
                            String provinceXX = "59";
                            if (responseAddressProvinceList.get(0).getProvinceXX() != null){
                                provinceXX = responseAddressProvinceList.get(0).getProvinceXX();
                            }
                            getAddressDistrict.setAddressDistrict(provinceXX);
                        }
                        break;
                    case 2:
                        List<ResponseAddressDistrict> responseAddressDistrictList
                                = ResponseAddressDistrict.FromJson(dataJSON);
                        if (responseAddressDistrictList != null && responseAddressDistrictList.size() > 0){
                            String districtXX = "124";
                            if (responseAddressDistrictList.get(0).getDistrictXX() != null){
                                districtXX = responseAddressDistrictList.get(0).getDistrictXX();
                            }
                            getAddressWard.setAddressWard(districtXX);
                        }
                        break;
                }
            }
        } else {
            System.out.println("Response error body: \n" + data.asString());
            Assert.assertEquals(statusCode, 200);
            Reporter.log("Output " + data.asString() + "");
        }
    }

    //accessToken temp
    private void accessTokenTemp (JSONObject jsonObject) {
        ClientResource.mToken = jsonObject.getJSONObject("data").getString("accessToken");
    }

    @AfterTest
    public void tearDown() {
        //mDriver.quit();
    }

}
